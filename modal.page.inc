<?php

/**
 * @file
 * Contains modal.page.inc.
 *
 * Page callback for Modal entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Modal templates.
 *
 * Default template: modal.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *     fields attached to the modal.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_modal(array &$variables) {
  $variables['modal'] = $variables['elements']['#modal'];
  /** @var \Drupal\ik_modals\Entity\ModalInterface $modal */
  $modal = $variables['modal'];

  
  // Make title field available to display.
  if ($modal->getTitle()) {
    $variables['label'] = $modal->getTitle();
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
