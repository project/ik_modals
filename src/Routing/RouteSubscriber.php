<?php

namespace Drupal\ik_modals\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('entity.modal.collection');
    if ($route) {
      $route->setDefaults([
        '_title' => 'Modals',
        '_entity_list' => 'modal',
      ]);
      $route->setRequirements([
        '_permission' => 'view modal overview',
      ]);
    }
  }

}
