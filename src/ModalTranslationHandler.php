<?php

namespace Drupal\ik_modals;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for modal.
 */
class ModalTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
