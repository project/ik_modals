<?php

namespace Drupal\ik_modals\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Modal entities.
 */
class ModalViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
