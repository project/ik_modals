<?php

namespace Drupal\ik_modals\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Modal type entities.
 */
interface ModalTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
