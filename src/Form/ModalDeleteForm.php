<?php

namespace Drupal\ik_modals\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Modal entities.
 *
 * @ingroup ik_modals
 */
class ModalDeleteForm extends ContentEntityDeleteForm {


}
