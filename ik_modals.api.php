<?php
use Drupal\ik_modals\Entity\Modal;

/**
 * hook_ik_modals_alter_module_settings
 * Alters the modal module sitewide settings
 *
 * @param [array] $settings
 * @return [array] $settings
 */
function hook_ik_modals_alter_module_settings(array &$settings) {
  // Set a custom geolocation service.
  $settings['geolocate'] = 'another_service';
}

/**
 * hook_ik_modals_alter_modal_settings
 * Alters individual modal settings
 *
 * @param \Drupal\ik_modals\Entity\Modal $entity
 * @param [array] $settings
 * @param [array] $geolocationData - see ModalService::getGeoloationData and hook_ik_modals_alter_user_geolocation
 * @return [array] $settings
 */
function hook_ik_modals_alter_modal_settings(Modal $entity, array &$settings, array $geolocationData) {
  // Make all modals of a specific bundle inactive.
  if ($entity->bundle() === 'general') {
    $settings['active'] = false;
  }

  // Force show a modal based on other data.
  // Add some conditions here and then set:
  $settings['showOverride'] = true;

  // If using hook_ik_modals_alter_user_geolocation
  // $geolocationData provides info on user's geolocation for conditions.
  // So you could potentially target postal codes or other information if it's available.
  
}

/**
 * hook_ik_modals_alter_user_geolocation
 * Alter the found (or not found) user's geolocation
 *
 * @param array $returnData
 * @return array $returnData
 */
function hook_ik_modals_alter_user_geolocation(array &$returnData) {
  // Alter the $returnData that gets saved in the private temporary storage. 
  $returnData['some_data'] = 'Some additional data';

  // Call another Geo-IP lookup service here to get country or state data.
  // Add it to $returnData.
  $returnData['country_code'] = 'US';
  $returnData['region_code'] = 'NC';
}